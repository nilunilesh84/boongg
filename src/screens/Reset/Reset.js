import { StyleSheet, Text, View, TextInput } from 'react-native'
import React, { useState } from 'react'
import CustomHeader from '../../components/CustomHeader'
import CustomButton from '../../components/CustomButton'
import { COLORS, FONTS, STYLES, RFONTS } from '../../constants/theme'
import { NAVIGATION } from '../../constants/routes'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'


const Reset = ({ navigation }) => {
    const [header, setHeader] = useState('Reset Password')
    const [password, setpassword] = useState('')
    const [otpField, setotpField] = useState(false)
    const [otp, setotp] = useState('')
    const [email, setemail] = useState('')
    const [resetField, setresetField] = useState(false)
    const [showpassword, setshowpassword] = useState(true)
    return (
        <View style={styles.container}>
            <CustomHeader header={header}
                leftshow={true}
                onPressLeft={() => {
                    if (header == "Verify") {
                        setHeader('Reset Password')
                        setotpField(false)
                    } else if (header == 'New Password') {
                        setresetField(false)
                        setHeader('Verify')
                    } else {
                        navigation.goBack()
                    }
                }}
            />
            {
                !otpField ?
                    <>
                        <View style={{ alignSelf: 'flex-start', marginVertical: 30 }}>
                            <Text style={{ ...RFONTS.h3 }}>Reset Your Password 🔒</Text>
                            <Text>Reset Your Password Reset Your PasswordReset Your PasswordReset Your PasswordReset Your Password</Text>
                        </View>
                        <View style={{ ...styles.input, borderColor: email ? COLORS.primary : 'white' }}>
                            <Feather name='mail' size={21} style={{ marginRight: 20 }} />
                            <TextInput value={email} onChangeText={t => setemail(t)} placeholder='Your email' style={{ width: '90%' }} />
                        </View>
                        <CustomButton
                            disabled={Boolean(email)}
                            title={'Send Verification Code'}
                            btnstyle={{ marginVertical: 53 }}
                            onPress={() => {
                                setotpField(true)
                                setHeader('Verify')
                            }} />
                    </> :
                    !resetField ?
                        <>
                            <View style={{ marginVertical: 32, alignSelf: 'flex-start' }}>
                                <Text style={styles.createtext}>Recovery Code 📲</Text>
                                <Text>Ceasdfljaksdflasdflaskdfalsdfaksda</Text>
                                <Text>Ceasdfljaksdflasdflaskdfalsdfaksda</Text>
                            </View>
                            <View style={{ ...styles.input, borderColor: otp ? COLORS.primary : 'white' }}>
                                <Feather name='mail' size={21} style={{ marginRight: 20 }} />
                                <TextInput value={otp} onChangeText={t => setotp(t)} placeholder='Your email' style={{ width: '90%' }} />
                            </View>
                            <CustomButton
                                title={'Verify'}
                                disabled={Boolean(otp)}
                                btnstyle={{ marginVertical: 53 }}
                                onPress={() => {
                                    setresetField(true)
                                    setHeader('New Password')
                                }} />
                        </> :
                        <>
                            <View style={{ alignSelf: 'flex-start', marginVertical: 30 }}>
                                <Text style={{ ...RFONTS.h3 }}>Set Your Password 🔐</Text>
                                <Text>Reset Your Password Reset Your PasswordReset Your PasswordReset Your PasswordReset Your Password</Text>
                            </View>
                            <View style={{ ...styles.input, borderColor: password ? COLORS.primary : 'white' }}>
                                <MaterialCommunityIcons name='lock-outline' size={21} />
                                <TextInput value={password} onChangeText={e => setpassword(e)} secureTextEntry={showpassword} placeholder='Your password' style={{ width: '90%' }} />
                                <FontAwesome5 onPress={() => setshowpassword(!showpassword)} style={styles.searchIcon} name={showpassword ? "eye" : "eye-slash"} color='#5A5A5A' />
                            </View>
                            <CustomButton
                                disabled={Boolean(password)}
                                title={'Set new password'}
                                btnstyle={{ marginVertical: 53 }}
                                onPress={() => navigation.navigate(NAVIGATION.LOGIN)} />
                        </>

            }
        </View>
    )
}

export default Reset

const styles = StyleSheet.create({
    container: {
        ...STYLES, padding: 18
    }, createtext: {
        fontSize: 22,
        color: COLORS.black,
        alignSelf: 'flex-start'
    }, input: {
        backgroundColor: "#FFFF",
        flexDirection: 'row',
        alignItems: 'center',
        width: '95%',
        borderRadius: 20,
        paddingHorizontal: 22,
        height: 55,
        borderColor: COLORS.primary,
        borderWidth: 2, marginTop: 70
    }
})