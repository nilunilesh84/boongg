import * as React from 'react';
import { Appbar } from 'react-native-paper';
import { Platform } from 'react-native';

const MORE_ICON = Platform.OS === 'ios' ? 'dots-horizontal' : 'dots-vertical';

const Settings = () => (
  <Appbar.Header>
    <Appbar.Content title="Title"  />
    <Appbar.Action icon="menu" onPress={() => { }} />
    <Appbar.Action icon={MORE_ICON} onPress={() => { }} />
  </Appbar.Header>
);

export default Settings;