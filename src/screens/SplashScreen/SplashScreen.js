import { Image, View, Text } from 'react-native'
import React from 'react'
import { SIZES } from '../../constants/theme'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useDispatch, } from 'react-redux'
import { userLogin } from '../../redux/actions/userActions';
import { NAVIGATION } from '../../constants/routes'
const SplashScreen = ({ navigation }) => {
  const dispatch = useDispatch()
  setTimeout(async () => {
    try {
      const user = await AsyncStorage.getItem('userDetails')
      console.log(user,'user')
      if (!user) {
        navigation.replace(NAVIGATION.AUTH)
      } else {
        navigation.replace(NAVIGATION.HOME_N)
      }
    } catch (error) {
      console.log(error)
    }
  }, 2000);
  return (
    <View style={{ flex: 1, backgroundColor: 'black', alignItems: 'center' }}>
      <Image resizeMode='contain' source={require('../../assets/logo.png')} style={{ flex: 1, width: SIZES.width }} />
      <Text style={{ color: 'white' }}>Please Wait, We are setting up</Text>
      <Text style={{ color: 'white' }}>Your App...</Text>
    </View>
  )
}

export default SplashScreen
