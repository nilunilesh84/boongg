import { BackHandler, StyleSheet, Text, View } from 'react-native'
import React, { useState, useEffect } from 'react'
import { Button } from 'react-native-paper'
import { useIsFocused } from "@react-navigation/native";
import { NAVIGATION } from '../../constants/routes';
const Profile = ({ navigation }) => {
  const [first, setfirst] = useState(1)
  const isFocused = useIsFocused();
  useEffect(() => {
    setfirst(1)
  }, [isFocused])
  BackHandler.addEventListener('hardwareBackPress', () => {
    navigation.navigate(NAVIGATION.HOME)
    return true;
  }, []);
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>{first}</Text>
      <Button onPress={() => setfirst(first + 1)}>add</Button>
      <Text onPress={() => navigation.navigate('Settings')}>Profile Screen</Text>
    </View>
  )
}

export default Profile

const styles = StyleSheet.create({})