import { StyleSheet, Text, View, TextInput, ScrollView, Image } from 'react-native'
import React, { useState } from 'react'
import { NAVIGATION } from '../../constants/routes'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { COLORS, SIZES } from '../../constants/theme'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import CustomButton from '../../components/CustomButton'
import { useDispatch } from 'react-redux';
import { userLogin } from '../../redux/actions/userActions';
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
const validationSchema = yup.object({
  username: yup.string().required('required'),
  password: yup.string().min(5, '4 character required').required('required'),
})
const Login = ({ navigation }) => {
  const dispatch = useDispatch()
  const [showpassword, setshowpassword] = useState(true)
  const [showError, setshowError] = useState(false)
  const { control, handleSubmit, formState: { errors, isValid } } = useForm({
    defaultValues: {
      username: 'kothrud@boongg.com',
      password: 'kothrud001',
    },
    resolver: yupResolver(validationSchema),
    mode: 'all'
  });
  const onSubmit = (formdata) => {
    const changeNavigation = (d) => {
      console.log(d, 'd')
      if (d === true) {
        setshowError(d)
      } else {
        setshowError(d)
        navigation.replace(NAVIGATION.HOME_N)
      }
    }
    dispatch(userLogin(formdata, changeNavigation))
  };
  return (
    <ScrollView keyboardShouldPersistTaps='always' >
      <View style={{ flex: 1, alignItems: 'center', padding: 12, backgroundColor: 'black', height: SIZES.height }}>
        {/* Image */}
        <Image resizeMode='contain' source={require('../../assets/logo.png')} style={{ flex: 1, width: SIZES.width * .9 }} />
        {/* form */}
        <View style={{ flex: 1, width: SIZES.width, alignItems: 'center' }}>
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <View style={{ ...styles.input }}>
                <TextInput
                  style={{ width: '90%' }}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  placeholder='Email id'
                />
              </View>
            )}
            name="username"
          />
          {errors.username && <Text style={{ color: 'red' }}>{errors.username.message}</Text>}
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <View style={{ ...styles.input }}>
                <TextInput
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  placeholder='Password'
                  secureTextEntry={showpassword}
                  style={{ width: '90%' }} />
                <FontAwesome5 size={25} onPress={() => setshowpassword(!showpassword)} name={showpassword ? "eye" : "eye-slash"} color='#5A5A5A' />
              </View>
            )}
            name="password"
          />
          <View style={{ width: '85%' }}>
            {showError && <Text style={{ color: 'white', alignSelf: 'flex-start' }}>Invalid Email id or Password</Text>}
          </View>
          <CustomButton
            disabled={true}
            btnstyle={{ backgroundColor: isValid ? 'red' : '#726162', width: '85%', top: 15 }}
            onPress={handleSubmit(onSubmit)}
            title={'LOGIN'} />
        </View>
        <View style={{ flex: 1 }} />
      </View>
    </ScrollView>
  )
}


const styles = StyleSheet.create({
  input: {
    backgroundColor: "#FFFF",
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
    borderRadius: 10,
    paddingHorizontal: 12,
    height: 55,
    marginVertical: 12,
    justifyContent: 'space-between'
  },
})

export default Login