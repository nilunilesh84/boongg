import { View, StyleSheet, Text, FlatList, Image, Pressable } from 'react-native'
import React, { useState } from 'react'
import { Appbar } from 'react-native-paper';
import { COLORS, FONTS, FSTYLES, SIZES, STYLES } from '../../constants/theme'
import { HomeItems } from './HomeItem';
import LogoutComponent from '../../components/LogoutComponent';

const Home = ({ navigation }) => {
  const [logoutdialogVisible, setlogoutdialogVisible] = useState(false)
  const renderItem = ({ item }) => {
    return (
      <Pressable onPress={() => alert(item.name)} style={styles.eachItem} >
        <Image resizeMode='contain' style={{ width: '60%', height: '60%' }} source={item.image} />
        <Text style={FONTS.h5}>{item.name}</Text>
      </Pressable>
    )
  }

  return (
    <View style={{ flex: 1 }}>
      <Appbar.Header style={{ backgroundColor: COLORS.primary, elevation: 0 }}>
        <Appbar.Action icon={'menu'} onPress={() => navigation.openDrawer()} />
        <Appbar.Content title='Boongg' />
      </Appbar.Header>
      <LogoutComponent
        visible={logoutdialogVisible}
        heading={'Confirm Logout'}
        button2={'Logout'}
        body={'Are you sure want to Logout ?'}
        onPress={() => setlogoutdialogVisible(!logoutdialogVisible)}
        onPress2={() => {
          setlogoutdialogVisible(!logoutdialogVisible)
        }} />
      <View style={{ flex: 1, alignItems: 'center' }}>
        {/* topBox */}
        <View style={styles.topBox}>
          <Text style={{ ...FONTS.h5, fontWeight: 'bold', top: -8 }} >Approx Bussiness This Month</Text>
          <View style={{ ...FSTYLES, width: '90%' }}>
            <View style={{ alignItems: 'center' }}>
              <Text>25</Text>
              <Text style={styles.fontStyle}>Booking</Text>
            </View>
            <View style={{ width: 2, height: '100%', backgroundColor: COLORS.black }} />
            <View style={{ alignItems: 'center' }}>
              <Text>Rs .11,222,225</Text>
              <Text style={styles.fontStyle}>Total Earning</Text>
            </View>
          </View>
        </View>
        {/* itemBox */}
        <View style={{ height: SIZES.width * .29 * 3, width: SIZES.width, alignItems: 'center' }}>
          <FlatList
            horizontal={false}
            data={HomeItems}
            numColumns={3}
            renderItem={renderItem}
            keyExtractor={(item, index) => index} />
        </View>
        {/* contactBox */}
        <View style={{ ...styles.topBox, height: '10%' }}>
          <Text style={styles.fontStyle}>Please Contact for any queries</Text>
          <Text style={styles.fontStyle}>9623876634</Text>
        </View>
      </View>
    </View>
  )
}

export default Home

const styles = StyleSheet.create({
  topBox: {
    ...STYLES,
    width: SIZES.width * .9,
    height: '18%',
    backgroundColor: '#fff',
    elevation: 4, borderRadius: 6,
    marginVertical: 25,
    padding: 8
  },
  eachItem: {
    ...STYLES,
    borderColor: COLORS.black,
    borderWidth: 0.5,
    width: SIZES.width * .29,
    height: SIZES.width * .29,
  },
  fontStyle: { ...FONTS.h5, fontWeight: 'bold' }
})