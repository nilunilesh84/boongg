export const HomeItems = [
    {
        name: 'Current Booking',
        image: require('../../assets/reading.png'),
       // onClick: NAVIGATION
    },
    {
        name: 'Create Booking',
        image: require('../../assets/notebook.png'),
        // onClick: NAVIGATION
    },
    {
        name: 'Cancelled Booking',
        image: require('../../assets/cancel_b.png'),
        // onClick: NAVIGATION
    },
    {
        name: 'Rent Calculator',
        image: require('../../assets/calculator.png'),
        // onClick: NAVIGATION
    },
    {
        name: 'Vehicle Inventory',
        image: require('../../assets/vespa.png'),
        // onClick: NAVIGATION
    },
    {
        name: 'Offers',
        image: require('../../assets/banking.png'),
        // onClick: NAVIGATION
    },
    {
        name: 'SOP',
        image: require('../../assets/management.png'),
        // onClick: NAVIGATION
    },
    {
        name: 'Log out',
        image: require('../../assets/exit.png'),
        // onClick: NAVIGATION
    },
]