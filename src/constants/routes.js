
export const NAVIGATION = {
    HOME: 'Home',
    REGISTER: 'Register',
    LOGIN: 'Login',
    PROFILE: 'Profile',
    MY_ACCOUNT: 'Myaccount',
    SETTINGS: 'Settings',
    SPLASH: 'SplashScreen',
    VERIFY: 'Verify',
    RESET: 'Reset',
    DRAWER: 'DrawerNavigator',
    HOME_N: 'HomeNavigator',
    AUTH: 'AuthNavigator',
}