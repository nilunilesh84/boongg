
export const START_LOADER = 'START_LOADER'
export const STOP_LOADER = 'STOP_LOADER'


export const startLoader = () => async dispatch => {
    dispatch({ type: START_LOADER })
}
export const stopLoader = () => async dispatch => {
    dispatch({ type: STOP_LOADER })
}