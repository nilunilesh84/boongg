import AsyncStorage from "@react-native-async-storage/async-storage"
import axios from "axios"

export const SET_USER = "SET_USER"
export const LOG_OUT = "LOG_OUT"

const BASE_URL = 'http://dev-api.boongg.com:3100/api'

export const loginUrl = BASE_URL + '/login';

export const userLogin = (formdata, fn) => async dispatch => {
    var userpass = JSON.stringify({
        "username": formdata.username,
        "password": formdata.password
    });
    var config = {
        method: 'post',
        url: loginUrl,
        headers: {
            'Accept': 'application/json, text/plain, /',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Authorization': '',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json',
            'Origin': 'http://dev.admin.boongg.com.s3-website.ap-south-1.amazonaws.com',
            'Referer': 'http://dev.admin.boongg.com.s3-website.ap-south-1.amazonaws.com/',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
            'charset': 'UTF-8'
        },
        data: userpass
    }
    try {
        const { data } = await axios(config)
        console.log(data)
        if (fn) fn(false)
        await AsyncStorage.setItem('userDetails', JSON.stringify(data.token))
    } catch (error) {
        if (fn) fn(true)
        console.log(error)
    }
    // dispatch({ type: SET_USER, payload: email })
}
export const logout = () => async dispatch => {
    dispatch({ type: LOG_OUT })
    await AsyncStorage.clear()
}
