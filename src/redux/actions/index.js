import axios from "axios";
import { Store } from '../store'
import { startLoader, stopLoader } from "./loaderAction";

export * from './userActions'


axios.interceptors.request.use(
  (req) => {
    Store.dispatch(startLoader());
    return req;
  },
  (err) => {
    if (err && err.response && err.response.data) {
      console.log(err.response.data.error)
    } else {
      console.log(err.message)
    }
    Store.dispatch(stopLoader());
    return err;
  }
)

axios.interceptors.response.use(
  (res) => {
    Store.dispatch(stopLoader());
    return res;
  },
  (err) => {
    if (err && err.response && err.response.data) {
      console.log(err.response.data.error)
    } else {
      console.log(err.message)
    }
    Store.dispatch(stopLoader());
    return err;
  }
);