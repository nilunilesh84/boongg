import { STOP_LOADER, START_LOADER } from "../actions/loaderAction"

const initState = {
    loaderShow: false
}


export default function loaderReducer(state = initState, action) {
    const { payload, type } = action
    switch (type) {
        case START_LOADER: return { ...state, loaderShow: true }
        case STOP_LOADER: return { ...state, loaderShow: false }
        default: return state
    }
}