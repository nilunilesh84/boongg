import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import userReducer from './reducers/userReducer'
import loaderReducer from './reducers/loaderReducer'


const rootReducer = combineReducers({
    userReducer,
    loaderReducer,
})

export const Store = createStore(rootReducer, applyMiddleware(thunk))
