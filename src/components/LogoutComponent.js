import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'
import { Button, Dialog, Portal } from 'react-native-paper';
const { width, height } = Dimensions.get('window');
const LogoutComponent = ({ visible, onPress, onPress2, heading, body, button2 }) => {
    return (
        <Portal>
            <Dialog style={{ height: height / 4, justifyContent: 'space-between', padding: 25 }} visible={visible}>
                <Text allowFontScaling={false} style={{ alignSelf: 'center', fontSize: 20, color: 'black', fontWeight: '600' }}>{heading}</Text>
                <Text allowFontScaling={false} style={{ alignSelf: 'center', fontSize: 18, color: 'black', fontWeight: '600' }}>{body}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Button uppercase={false} onPress={() => onPress()}
                        mode='text' labelStyle={{ color: 'red', fontWeight: '600' }} >
                        <Text style={{ fontSize: 11, }} allowFontScaling={false}>Cancel</Text>
                    </Button>
                    <Button uppercase={false} style={{ borderRadius: 18 }} onPress={() => onPress2()}
                        mode='contained' contentStyle={{ backgroundColor: 'red' }}>
                        <Text style={{ fontSize: 11, }} allowFontScaling={false}>{button2}</Text>
                    </Button>
                </View>
            </Dialog>
        </Portal>
    )
}

export default LogoutComponent

const styles = StyleSheet.create({})