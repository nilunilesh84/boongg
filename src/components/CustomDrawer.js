import { Text, View, Pressable, Image } from 'react-native';
import React from 'react';
import {
    DrawerContentScrollView,
    DrawerItemList,
} from '@react-navigation/drawer';
const CustomDrawer = props => {
    return (
        <View style={{ flex: 1 }}>
             <DrawerContentScrollView style={{ height: '40%' }} {...props}>
                <DrawerItemList {...props} />
            </DrawerContentScrollView>
            <View style={{ height: '30%', justifyContent: 'center', flexDirection: 'row', alignItems: 'flex-end' }}>
                <Text>Logout</Text>
            </View>
        </View>
    );
};

export default CustomDrawer;

