import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NAVIGATION } from '../constants/routes'
import Home from '../screens/Home'
import CustomDrawer from '../components/CustomDrawer'

const Drawer = createDrawerNavigator();

export default function DrawerNavigator() {
  return (
      <Drawer.Navigator useLegacyImplementation  drawerContent={props => <CustomDrawer {...props} />} screenOptions={{ headerShown: false, }}>
        <Drawer.Screen name={NAVIGATION.HOME} component={Home} />
      </Drawer.Navigator>
  );
}

