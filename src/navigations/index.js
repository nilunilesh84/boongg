import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import AuthNavigator from './AuthNavigator'
import HomeNavigator from './HomeNavigator'
import SplashScreen from '../screens/SplashScreen'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NAVIGATION } from '../constants/routes'
const AppNavigator = () => {
  
  const Stack = createNativeStackNavigator();
   return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name={NAVIGATION.SPLASH} component={SplashScreen} />
        <Stack.Screen name={NAVIGATION.HOME_N} component={HomeNavigator} />
        <Stack.Screen name={NAVIGATION.AUTH} component={AuthNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default AppNavigator
