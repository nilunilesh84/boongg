import React from 'react'
import { StyleSheet} from 'react-native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Register from '../screens/Register'
import Login from '../screens/Login'
import Reset from '../screens/Reset'
import Verify from '../screens/Verify';
import { NAVIGATION } from '../constants/routes';
const AuthNavigator = () => {

  const AuthStack = createNativeStackNavigator();
  return (
    <AuthStack.Navigator screenOptions={{ headerShown: false }}>
      <AuthStack.Screen name={NAVIGATION.LOGIN} component={Login} />
      <AuthStack.Screen name={NAVIGATION.REGISTER} component={Register} />
      <AuthStack.Screen name={NAVIGATION.VERIFY} component={Verify} />
      <AuthStack.Screen name={NAVIGATION.RESET} component={Reset} />
    </AuthStack.Navigator>
  )
}

export default AuthNavigator

const styles = StyleSheet.create({})